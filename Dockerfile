FROM haproxy:alpine

ENV DOCKER_BASE_VERSION=0.0.4 \
    CONSUL_TEMPLATE_VERSION=0.18.2 \
    DOMAIN="lb.service.consul" \
    ALT_DOMAIN="" \
    USESSL=1 \
    CACERT=ca.pem \
    CERT=user-services.pem \
    KEY=user-services-key.pem \
    CONTROL_CERT="" \
    CONTROL_KEY="" \
    CONTROL_PORT=4443 \
    CONSUl_HOST="" \
    CONSUL_PORT=8500 \
    CONSUL_SSL=0 \
    CONSUL_CACERT=ca.pem \
    CONSUL_CERT=cert.pem \
    CONSUL_KEY=cert-key.pem \
    CONSUL_API_TOKEN= \
    MAX_CONNECTIONS=256

RUN addgroup haproxy && \
    adduser -S -G haproxy haproxy

# Set up certificates, our base tools, and Consul.
RUN apk add --no-cache ca-certificates gnupg openssl && \
    gpg --recv-keys 91A6E7F85D05C65630BEF18951852D87348FFC4C && \
    mkdir -p /tmp/build && \
    cd /tmp/build && \
    wget https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_linux_amd64.zip && \
    wget https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS && \
    wget https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS.sig && \
    gpg --batch --verify docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS.sig docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS && \
    grep ${DOCKER_BASE_VERSION}_linux_amd64.zip docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS | sha256sum -c && \
    unzip docker-base_${DOCKER_BASE_VERSION}_linux_amd64.zip && \
    wget https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip && \
    wget https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_SHA256SUMS && \
    wget https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_SHA256SUMS.sig && \
    gpg --batch --verify consul-template_${CONSUL_TEMPLATE_VERSION}_SHA256SUMS.sig consul-template_${CONSUL_TEMPLATE_VERSION}_SHA256SUMS && \
    grep ${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip consul-template_${CONSUL_TEMPLATE_VERSION}_SHA256SUMS | sha256sum -c && \
    unzip consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip && \
    cp bin/gosu bin/dumb-init consul-template /bin && \
    cd /tmp && \
    rm -rf /tmp/build && \
    apk del gnupg && \
    rm -rf /root/.gnupg

EXPOSE 80 443

VOLUME /certs

COPY ./entrypoint.sh /
COPY ./proxy.sh /

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/proxy.sh"]
