#!/bin/dumb-init /bin/sh
set -e

if [ "$USESSL" == 1 ] || [ "$USESSL" == "force" ]; then
    if [ -z "$CACERT" ] || [ ! -f "/certs/$CACERT" ]; then
        echo "CACERT is not set" >&2
        exit 1
    fi

    if [ -z "$CERT" ] || [ ! -f "/certs/$CERT" ]; then
        echo "CERT is not set" >&2
        exit 1
    fi
    if [ -z "$KEY" ] || [ ! -f "/certs/$KEY" ]; then
        echo "KEY is not set" >&2
        exit 1
    fi
fi

if [ "$CONSUL_USESSL" == 1 ]; then
    if [ -z "$CONSUL_CACERT" ] || [ ! -f "/certs/$CONSUL_CACERT" ]; then
        echo "CONSUL_CACERT is not set" >&2
        exit 1
    fi

    if [ -z "$CONSUL_CERT" ] || [ ! -f "/certs/$CONSUL_CERT" ]; then
        echo "CONSUL_CERT is not set" >&2
        exit 1
    fi
    if [ -z "$CONSUL_KEY" ] || [ ! -f "/certs/$CONSUL_KEY" ]; then
        echo "CONSUL_KEY is not set" >&2
        exit 1
    fi
fi

if [ -z "$CONSUL_HOST" ]; then
    echo "CONSUL_HOST is not set" >&2
    exit 1
fi

if [ -z "$CONSUL_PORT" ]; then
    echo "CONSUL_PORT is not set" >&2
    exit 1
fi

exec "$@"
