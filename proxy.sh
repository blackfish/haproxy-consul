#!/bin/sh -e
BASEDIR=$(readlink -f $(dirname $0))

DOMAIN=${DOMAIN:-lb.service.consul}
ALT_DOMAIN=${ALT_DOMAIN}
CONSUl_HOST=${CONSUL_HOST}
CONSUL_PORT=${CONSUL_PORT:-8500}
USESSL=${USESSL:-1}
CACERT=${CACERT:-ca.pem}
CERT=${CERT:-cert.pem}
KEY=${KEY:-cert-key.pem}
CONTROL_PORT=${CONTROL_PORT:-4443}

MAX_CONNECTIONS=${MAX_CONNECTIONS:-256}

CONSUL_USESSL=${CONSUL_USESSL:-1}
CONSUL_CACERT=${CONSUL_CACERT:-ca.pem}
CONSUL_CERT=${CONSUL_CERT:-cert.pem}
CONSUL_KEY=${CONSUL_KEY:-cert-key.pem}
CONSUL_API_TOKEN=${CONSUL_API_TOKEN:-}

if [ -z "$CONSUL_HOST" ]; then
    echo "CONSUL_HOST not set." >&2
    exit 1
fi

if [ "$USESSL" == 1 ] || [ "$USESSL" == "force" ]; then
    SSLCERT=$(mktemp)
    cat /certs/$CERT /certs/$KEY > $SSLCERT
    if [ ! -z "$CONTROL_CERT" ]; then
        CONTROLSSLCERT=$(mktemp)
        cat /certs/$CONTROL_CERT /certs/$CONTROL_KEY > $CONTROLSSLCERT
    fi
elif [ "$USESSL" == 0 ]; then
    unset USESSL
fi

cat > /tmp/haproxy.tmpl <<EOF
## adapted from https://github.com/CiscoCloud/haproxy-consul/blob/master/template/consul.tmpl
global
    maxconn $MAX_CONNECTIONS
    # Recommended SSL ciphers as per https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
    ssl-default-bind-options no-sslv3
    ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS

    ssl-default-server-options no-sslv3
    ssl-default-server-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS
    tune.ssl.default-dh-param 2048

defaults
    timeout connect "5000ms"
    timeout client "50000ms"
    timeout server "50000ms"

###
# HTTP START
###

# HTTP FRONTENDS
##
frontend http
    mode http
    option forwardfor
    option http-server-close
    bind *:80
    reqadd X-Forwarded-Proto:\ http if !{ ssl_fc }
    {{ if eq (env "USESSL") "force" }}
    # Redirect all non-secure connections to HTTPS
    redirect scheme https if !{ ssl_fc }{{ end }}

    # Generated automatically by consul-template
    {{ range \$s := services }}{{ if in \$s.Tags "http"}}acl host_{{ \$s.Name }}_http hdr(host) -i {{ \$s.Name }}.{{ env "DOMAIN" }}
    use_backend {{ \$s.Name }}_http_backend if host_{{ \$s.Name }}_http
    {{ range \$tag := \$s.Tags }}{{ if \$tag | regexMatch "pubdomain_([-.a-z]+)"}}{{ with \$pubdomain := \$tag | regexReplaceAll "pubdomain_([-.a-z]+)" "\$1" }}
    acl host_{{ \$s.Name }}_http_{{ \$pubdomain }} hdr(host) -i {{ \$pubdomain }}
    use_backend {{ \$s.Name }}_http_backend if host_{{ \$s.Name }}_http_{{ \$pubdomain }}
    {{ end }}{{ end }}{{ end }}{{ end }}{{ end }}
EOF
if [ ! -z "$ALT_DOMAIN" ]; then
    cat >> /tmp/haproxy.tmpl <<EOF
    {{ range services }}{{ if in .Tags "http" }}acl alt_host_{{ .Name }}_http hdr(host) -i {{ .Name }}.{{ env "ALT_DOMAIN" }}
    use_backend {{ .Name }}_http_backend if alt_host_{{ .Name }}_http
    {{ end }}{{ end }}
EOF
fi
cat >> /tmp/haproxy.tmpl <<EOF
# HTTP BACKENDS
##
{{ range services }}
{{ if in .Tags "http" }}backend {{ .Name }}_http_backend
   mode http
   {{ if in .Tags "sticky" }}stick-table type ip size 200k expire 30m
   stick on src{{ end }}
   {{ range service .Name "passing,warning"}}server {{ .Node }}-{{ .Port }} {{ .Address }}:{{ .Port }}
{{ end }}
{{ end }}{{ end }}
###
# HTTP END
###

{{ if env "USESSL" }}
###
# HTTPS START
###

# HTTPS FRONTENDS
###
frontend https
    mode http
    option forwardfor
    option http-server-close
    bind *:443 ssl crt $SSLCERT
    reqadd X-Forwarded-Proto:\ https if { ssl_fc }
    # Generated automatically by consul-template
    {{ range \$s := services }}{{ if in \$s.Tags "https"}}acl host_{{ \$s.Name }}_https hdr(host) -i {{ \$s.Name }}.{{ env "DOMAIN" }}
    use_backend {{ \$s.Name }}_https_backend if host_{{ \$s.Name }}_https
    {{ range \$tag := \$s.Tags }}{{ if \$tag | regexMatch "pubdomain_([-.a-z]+)"}}{{ with \$pubdomain := \$tag | regexReplaceAll "pubdomain_([-.a-z]+)" "\$1" }}
    acl host_{{ \$s.Name }}_https_{{ \$pubdomain}} hdr(host) -i {{ \$pubdomain }}
    use_backend {{ \$s.Name }}_https_backend if host_{{ \$s.Name }}_https_{{ \$pubdomain }}
    {{ end }}{{ end }}{{ end }}{{ end }}{{ end }}
EOF
if [ ! -z "$ALT_DOMAIN" ]; then
    cat >> /tmp/haproxy.tmpl <<EOF
    {{ range services }}{{ if in .Tags "https"}}acl alt_host_{{ .Name }}_https hdr(host) -i {{ .Name }}.{{ env "ALT_DOMAIN" }}
    use_backend {{ .Name }}_https_backend if alt_host_{{ .Name }}_https
    {{ end }}{{ end }}
EOF
fi
cat >> /tmp/haproxy.tmpl <<EOF
# HTTPS BACKENDS
###
{{ range services }}
{{ if in .Tags "https"}}backend {{ .Name }}_https_backend
   mode http
   {{ if in .Tags "sticky" }}stick-table type ip size 200k expire 30m
   stick on src{{ end }}
   {{ range service .Name "passing,warning"}}server {{ .Node }}-{{ .Port }} {{ .Address }}:{{ .Port }} ssl ca-file /certs/$CACERT crt $SSLCERT
{{ end }}
{{ end }}{{ end }}
###
# HTTPS END
###
{{ end }}

###
# TCP START
###
{{ range \$service := services }}{{ if in \$service.Tags "tcp"}}{{range \$tag := \$service.Tags}}{{ if ( \$tag | regexMatch "port:[0-9]+") }}

# TCP {{\$service.Name}} START
###
frontend {{ \$service.Name }}_tcp
   mode tcp
   bind *:{{ \$tag | replaceAll "port:" "" }}
   use_backend {{ \$service.Name }}_tcp_backend

backend {{ \$service.Name }}_tcp_backend
   mode tcp
   {{ if in \$service.Tags "sticky" }}stick-table type ip size 200k expire 30m
   stick on src{{ end }}
   {{ range service \$service.Name "passing,warning" }}server {{ .Node }}-{{ .Port }} {{ .Address }}:{{ .Port }}
{{ end }}{{ end }}{{ end }}{{ end }}{{ end }}
EOF

if [ ! -f "$CONTROL_CERT" ]; then
    cat >> /tmp/haproxy.tmpl <<EOF
    {{ if env "USESSL" }}
    ###
    # CONTROL HTTPS START
    ###

    # CONTROL HTTPS FRONTENDS
    ###
    frontend control
    mode http
    option forwardfor
    option http-server-close
    bind *:$CONTROL_PORT ssl crt $CONTROLSSLCERT
    reqadd X-Forwarded-Proto:\ https if { ssl_fc }
    # Generated automatically by consul-template
    {{ range services }}{{ if in .Tags "control" }}acl host_{{ .Name }}_https hdr(host) -i {{ .Name }}.{{ env "DOMAIN" }}:$CONTROL_PORT
    use_backend {{ .Name }}_control_backend if host_{{ .Name }}_https
    {{ end }}{{ end }}
EOF
    if [ ! -z "$ALT_DOMAIN" ]; then
        cat >> /tmp/haproxy.tmpl <<EOF
    {{ range services }}{{ if in .Tags "control" }}acl alt_host_{{ .Name }}_https hdr(host) -i {{ .Name }}.{{ env "ALT_DOMAIN" }}:$CONTROL_PORT
    use_backend {{ .Name }}_control_backend if alt_host_{{ .Name }}_https
    {{ end }}{{ end }}
EOF
    fi
    cat >> /tmp/haproxy.tmpl <<EOF

    # HTTPS CONTROL-BACKENDS
    ###
    {{ range services }}
    {{ if in .Tags "control" }} backend {{ .Name }}_control_backend
    mode http
    {{ if in .Tags "sticky" }}stick-table type ip size 200k expire 30m
    stick on src{{ end }}
    {{ range service .Name "passing,warning"}}server {{ .Node }}-{{ .Port }} {{ .Address }}:{{ .Port }} ssl ca-file /certs/$CACERT crt $CONTROLSSLCERT
    {{ end }}
    {{ end }}{{ end }}
    ###
    # HTTPS END
    ###
    {{ end }}
EOF
fi

touch /tmp/cfg
cat > /tmp/consul-template.conf <<EOF
consul {
  address = "$CONSUL_HOST:$CONSUL_PORT"
EOF

if [ ! -z "$CONSUL_API_TOKEN" ]; then
    cat >> /tmp/consul-template.conf <<EOF
  token = "$CONSUL_API_TOKEN"
EOF
fi

if [ "$CONSUL_USESSL" == 1 ]; then
    cat >> /tmp/consul-template.conf <<EOF
  ssl {
    enabled = true
    verify = true
    cert = "/certs/$CONSUL_CERT"
    key = "/certs/$CONSUL_KEY"
    ca_cert = "/certs/$CONSUL_CACERT"
  }
EOF
fi

cat >> /tmp/consul-template.conf <<EOF
}

exec {
  command = "haproxy -p /tmp/pid -f /tmp/cfg "
}

template {
  source = "/tmp/haproxy.tmpl"
  destination = "/tmp/cfg"
}

deduplicate {
  enabled = true
  prefix = "consul-template/dedup/"
}
EOF

/bin/consul-template -config /tmp/consul-template.conf
